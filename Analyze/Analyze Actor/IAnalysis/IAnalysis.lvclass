﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Analyze Actor.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Analyze Actor.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Analysis Results.vi" Type="VI" URL="../Analysis Results.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!))!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$Z!=!!?!!!H%U&amp;O97RZ?G5A17.U&lt;X)O&lt;(:M;7)235&amp;O97RZ=WFT,GRW9WRB=X-!$5F"&lt;G&amp;M?8.J=S"P&gt;81!$U!$!!F*&lt;7&amp;H:3"/=CY!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"'1(!!(A!!(BR"&lt;G&amp;M?8JF)&amp;*F=X6M&gt;(-A1G&amp;T:3ZM&gt;G.M98.T!!!=17ZB&lt;(F[:3"3:8.V&lt;(2T)%*B=W5O&lt;(:D&lt;'&amp;T=Q!!'%"!!!(`````!!A+5G6T&gt;7RU=S"*&lt;A!!3E"Q!"Y!!#!?17ZB&lt;(F[:3"198*B&lt;76U:8)A1G&amp;T:3ZM&gt;G.M98.T!!!?17ZB&lt;(F[:3"198*B&lt;76U:8)A1G&amp;T:3ZM&gt;G.M98.T!!!91%!!!@````]!#AJ198*B&lt;76U:8*T!!!_1(!!(A!!*R."&lt;G&amp;M?8JF)%&amp;D&gt;'^S,GRW&lt;'FC%5F"&lt;G&amp;M?8.J=SZM&gt;G.M98.T!!R*17ZB&lt;(FT;8-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!9!"!!(!!E!#Q!-!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!%!!!!!!!!!!1!!!#%!!!!B!!!!#1!!!!!!%!$1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074274818</Property>
	</Item>
</LVClass>
